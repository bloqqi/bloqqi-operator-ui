#ifndef __ControlSystemMain_h_GUARD
#define __ControlSystemMain_h_GUARD

#include "ControlSystem.h"

Main_VARS get_state();
void *run_control_system(void *threadid);
long get_control_system_period();
void set_input_variable(const char *path, const char *value);

#endif