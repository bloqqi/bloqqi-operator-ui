#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <pthread.h>
#include <fcntl.h>
#include "web_server.h"

#define POSTBUFFERSIZE 4096
#define BUF_SIZE 1024
#define GET 0
#define POST 1

struct connection_info_struct {
	int connectiontype;
	char *id;
	char *value;
	struct MHD_PostProcessor *postprocessor;
};

static json_t *cache_io;
static json_t *cache_all;

static int send_response(struct MHD_Connection *connection,
		struct MHD_Response *response, unsigned int status_code) {
	if (!response)
		return MHD_NO;
	int ret = MHD_queue_response(connection, status_code, response);
	MHD_destroy_response(response);
	return ret;
}

static int iterate_post(
		void *coninfo_cls, enum MHD_ValueKind kind, const char *key,
		const char *filename, const char *content_type,
		const char *transfer_encoding, const char *data,
		uint64_t off,size_t size) {
	if(size ==0) {
		return MHD_NO;
	}
	struct connection_info_struct *con_info = coninfo_cls;
	if (strcmp(key, "id") == 0) {
		con_info->id = malloc(BUF_SIZE);
		strncpy(con_info->id, data, BUF_SIZE);
	} else if (strcmp(key, "value") == 0) {
		con_info->value = malloc(BUF_SIZE);
		strncpy(con_info->value, data, BUF_SIZE);
	}
	return MHD_YES;
}

static void request_completed(void *cls, struct MHD_Connection *connection,
		void **con_cls, enum MHD_RequestTerminationCode toe) {
	struct connection_info_struct *con_info = *con_cls;
	if (con_info == NULL)
		return;
	if (con_info->connectiontype == POST) {
		MHD_destroy_post_processor(con_info->postprocessor);
		if (con_info->id) free(con_info->id);
		if (con_info->value) free(con_info->value);
	}
	free (con_info);
	*con_cls = NULL;
}

static int init_connection(struct MHD_Connection *connection,
		const char *method,void **con_cls) {
	struct connection_info_struct *con_info;
	con_info = calloc(1, sizeof(struct connection_info_struct));
	if (con_info == NULL)
		return MHD_NO;
	if (strcmp(method, "POST") == 0) {
		con_info->postprocessor = MHD_create_post_processor(
			connection, POSTBUFFERSIZE,
			iterate_post, (void *) con_info);
		if (con_info->postprocessor == NULL) {
			free (con_info);
			return MHD_NO;
		}
		con_info->connectiontype = POST;
	} else {
		con_info->connectiontype = GET;
	}
	*con_cls = (void *) con_info;
	return MHD_YES;
}

static const char *get_mime_type(const char *file) {
	char *dot = strrchr(file, '.');
	if (dot) {
		if (strcmp(dot, ".html") == 0) {
			return "text/html";
		} else if (strcmp(dot, ".css") == 0) {
			return "text/css";
		} else if (strcmp(dot, ".js") == 0) {
			return "application/javascript";
		}
	}
	return "text/plain";
}

static struct MHD_Response* get_response_from_fd(const char *file) {
	struct MHD_Response *response;
	int fd = open(file, O_RDONLY);
	size_t size = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	response = MHD_create_response_from_fd(size, fd);
	MHD_add_response_header(response, "Content-Type", get_mime_type(file));
	return response;
}

/* cache json requests */
static json_t* get_json_cache(json_t **cache, json_t* (*get_json)(), long period) {
	if (*cache != NULL
			&& (*cache)->period == period) {
		return *cache;
	} else {
		if (*cache != NULL) {
			free((*cache)->s);
			free((*cache));
		}
		*cache = get_json();
		return *cache;
	}
}

static struct MHD_Response* get_api_response(server_callbacks *callbacks, const char *url) {
	json_t *json;
	long period = callbacks->get_period();
	if (strcmp(url, "/api/get-io") == 0) {
		json = get_json_cache(&cache_io, callbacks->get_io_json, period);
	} else {
		json = get_json_cache(&cache_all, callbacks->get_all_json, period);
	}
	return MHD_create_response_from_buffer(strlen(json->s), json->s, MHD_RESPMEM_PERSISTENT);
}

static struct MHD_Response* get_not_found_response(const char *url) {
	char *page = malloc(BUF_SIZE+strlen(url));
	sprintf(page, "<html><head></head><body><p>URL %s not found (404)</p></body>", url);
	return MHD_create_response_from_buffer(strlen(page), page, MHD_RESPMEM_MUST_FREE);
}


static int handle_get(server_callbacks *callbacks, 
		struct MHD_Connection *connection, const char *url) {
	struct MHD_Response *response;
	unsigned int status_code = MHD_HTTP_OK;

	if (strcmp(url, "/") == 0
			&& access("html/index.html", R_OK) == 0) {
		response = get_response_from_fd("html/index.html");
	} else if (strcmp(url, "/api/get-all") == 0 || strcmp(url, "/api/get-io") == 0) {
		response = get_api_response(callbacks, url);
	} else {
		char file[BUF_SIZE];
		snprintf(file, BUF_SIZE, "html/%s", url+1);
		if (strlen(url) > 0
				&& strstr(url+1, "/") == NULL
				&& access(file, R_OK) == 0) {
			response = get_response_from_fd(file);
		} else {
			status_code = MHD_HTTP_NOT_FOUND;
			response = get_not_found_response(url);
		}
	}

	return send_response(connection, response, status_code);
}

static int handle_post(server_callbacks *callbacks,
		struct MHD_Connection *connection, const char *url,
		const char *upload_data, size_t *upload_data_size, void **con_cls) {
	if (strcmp(url, "/api/set-input") == 0) {
		struct connection_info_struct *con_info = *con_cls;
		if (*upload_data_size != 0) {
			MHD_post_process(con_info->postprocessor, upload_data, *upload_data_size);
			*upload_data_size = 0;
			callbacks->set_input(con_info->id, con_info->value);
			return MHD_YES;
		} else {
			char *page = "OK\n";
			struct MHD_Response *response;
			response = MHD_create_response_from_buffer(strlen(page), page, MHD_RESPMEM_MUST_COPY);
			return send_response(connection, response, MHD_HTTP_OK);
		}
	}
	return MHD_NO;
}

static int answer_to_connection(void *cls, struct MHD_Connection *connection,
		const char *url, const char *method, const char *version,
		const char *upload_data, size_t *upload_data_size, void **con_cls) {
	server_callbacks *callbacks = (server_callbacks*) cls;

	if (strcmp(method, "GET") != 0 && strcmp(method, "POST")) {
		return MHD_NO; /* unexpected method */
	}

	if (*con_cls == NULL) {
		return init_connection(connection, method, con_cls);
	}

	if (strcmp(method, "GET") == 0) {
		return handle_get(callbacks, connection, url);
	} else if (strcmp(method, "POST") == 0) {
		return handle_post(callbacks, connection, url, upload_data, upload_data_size, con_cls);
	}

	return MHD_NO;
}


int start_webserver(int port, server_callbacks *callbacks) {
	printf("Listening on port %d\n", port);
	struct MHD_Daemon *daemon;
	daemon = MHD_start_daemon(
					MHD_USE_SELECT_INTERNALLY, // use single thread
					port,
					NULL,
					NULL,
					&answer_to_connection,
					callbacks,
					MHD_OPTION_NOTIFY_COMPLETED,
					request_completed,
					NULL,
					MHD_OPTION_END);
	if (daemon == NULL) {
		return 1;
	}
	sleep(-1);
	//MHD_stop_daemon(daemon);
	return 0;
}
