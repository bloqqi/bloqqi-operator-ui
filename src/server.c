#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <pthread.h>
#include <fcntl.h>
#include "ControlSystemMain.h"
#include "web_server.h"

json_t *get_io_json() {
	Main_VARS vars = get_state();
	long period = get_control_system_period();
	char *s = bloqqi_io_vars_to_json(&vars);
	// TODO: could overflow
	sprintf(s+strlen(s)-1, ", \"__period\": %ld}", period);
	json_t *json = malloc(sizeof(json_t));
	json->s = s;
	json->period = period;
	return json;
}
json_t *get_all_json() {
	Main_VARS vars = get_state();
	long period = get_control_system_period();
	char *s = bloqqi_all_vars_to_json(&vars);
	// TODO: could overflow
	sprintf(s+strlen(s)-1, ", \"__period\": %ld}", period);
	json_t *json = malloc(sizeof(json_t));
	json->s = s;
	json->period = period;
	return json;
}

int main(int argc, char ** argv) {
	if (argc != 2) {
		printf("%s <PORT>\n", argv[0]);
		return 1;
	}

	pthread_t thread;
	long t = 0;
	int rc = pthread_create(&thread, NULL, run_control_system, (void *)t);
	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	int port = atoi(argv[1]);
	server_callbacks *callbacks = malloc(sizeof(server_callbacks));
	callbacks->get_io_json = get_io_json;
	callbacks->get_all_json = get_all_json;
	callbacks->set_input = set_input_variable;
	callbacks->get_period = get_control_system_period;
	return start_webserver(port, callbacks);
}