#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>
#include "ControlSystem.h"
#include "InitialInputValues.h"

#define HZ (1)
#define T (1000000/HZ)

static pthread_mutex_t mutex;
static Main_VARS global_state = {};

Main_VARS get_state() {
	Main_VARS state;
	pthread_mutex_lock(&mutex);
	state = global_state;
	pthread_mutex_unlock(&mutex);
	return state;
}

void set_input_variable(const char *path, const char *value) {
	pthread_mutex_lock(&mutex);
	bloqqi_set_input_variable(&global_state, path, value);
	pthread_mutex_unlock(&mutex);
}

static useconds_t useconds() {
	struct timeval v;
	gettimeofday(&v, NULL);
	return v.tv_sec*1000000 + v.tv_usec;
}

static long period_index = 0;
long get_control_system_period() {
	long retval;
	pthread_mutex_lock(&mutex);
	retval = period_index;
	pthread_mutex_unlock(&mutex);
	return retval;
}

void *run_control_system(void *threadid) {
	threadid = NULL;  // Silent warning message...
	
	pthread_mutex_lock(&mutex);
	initialize_input_values(&global_state);
	pthread_mutex_unlock(&mutex);

	Main_VARS vars = {};
	useconds_t extra = 0;
	while(true) {
		useconds_t start = useconds();

		// Read input
		pthread_mutex_lock(&mutex);
		vars.input = global_state.input;
		pthread_mutex_unlock(&mutex);

		// Run Bloqqi program one period
		bloqqi_main(&vars);
		
		// Store states/output
		pthread_mutex_lock(&mutex);
		global_state.state = vars.state;
		global_state.output = vars.output;
		period_index++;
		pthread_mutex_unlock(&mutex);

		useconds_t end = useconds();
		useconds_t run_time = end - start;

		// Sleep
		start = end;
		useconds_t sleep_time = T - run_time - extra;
		assert(sleep_time > 0);
		usleep(sleep_time);
		end = useconds();
		useconds_t time_slept = end - start;
		if (run_time + time_slept > T) {
			extra = run_time + time_slept - T;
		} else {
			extra = 0;
		}
	}

	pthread_exit(NULL);
}