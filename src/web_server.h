#ifndef web_server_h
#define web_server_h

typedef struct {
	char *s;
	long period;
} json_t;

typedef struct {
	json_t* (*get_io_json)();
	json_t* (*get_all_json)();
	void (*set_input)(const char *name, const char *value);
	long (*get_period)();
} server_callbacks;


int start_webserver(int port, server_callbacks *callbacks);

#endif