#!/bin/sh
if [ ! -f bloqqi-compiler.jar ]; then
	echo "Building Bloqqi compiler"
	if [ ! -f bloqqi-compiler/build.xml ]; then
		git submodule init > /dev/null
		git submodule update > /dev/null
	fi
	(cd bloqqi-compiler && ant jar > /dev/null)
	cp bloqqi-compiler/bloqqi-compiler.jar .
fi

