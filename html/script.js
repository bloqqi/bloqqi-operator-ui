// State
var init = false;
var oldVars = null;

var showInput = true;
var showState = false;
var showOutput = true;

// callbacks for specific variables
let callbackMap = new Map()
// callbacks on all variables
let callbacks = new Array()
let inputFields = new Array()

function registerVariableCallback(v, f) {
	if (callbackMap.has(v)) {
		callbackMap.get(v).push(f);
	} else {
		callbackMap.set(v, [f]);
	}
}

function registerCallback(f) {
	callbacks.push(f)
}

function registerOutput(variable, id) {
	registerVariableCallback(
		variable,
		function(v) { $(getAccess(id)).html(v); }
	);
}

function registerInputField(id) {
	inputFields.push(id);
	$(getAccess(id))
		.keypress(inputPressEnter)
		.keyup(inputKeyUp);
}

function getAccess(id) {
	return "#" + id.replace(/\./g, '\\.');
}

function showVariable(k, v) {
	if (ignoreVariables.indexOf(k) >= 0)
		return false;
	if (v['type'] == 'input')
		return showInput;
	if (v['type'] == 'state')
		return showState;
	if (v['type'] == 'output')
		return showOutput
	return false;
}

function inputPressEnter(e) {
	if (e.which == 13) {
		$.post("/api/set-input",
			{id: e.target.id, value: e.target.value},
			function(data) { }
		);
	}
}

function inputKeyUp(e) {
	updateInputField(e.target.id, e.target.value);
}

function updateInputField(id, value) {
	var access = getAccess(id);
	if (oldVars != null && oldVars[id].value != value) {
		$(access).addClass("input-changed");
	} else {
		$(access).removeClass("input-changed");
	}
}

function createDivStructure(vars, keys) {
	$("#variables").html("");
	keys.forEach(function(key) {
		if (showVariable(key, vars[key])) {
			var html = "<div><label>" + key + ":</label>";
			if (vars[key]['type'] == 'input') {
				html += "<input id=\"" + key + "\" />";
			} else {
				html += "<span id=\"" + key + "\">&nbsp;</span>";
			}
			html += "</div>";
			$("#variables").append(html);
		}
	});
	$(':input').keypress(inputPressEnter);
	$(':input').keyup(inputKeyUp);
}

function updateDivContent(vars, keys) {
	keys.forEach(function(key) {
		if (showVariable(key, vars[key])) {
			if (oldVars == null || vars[key]['value'] != oldVars[key]['value']) {
				var access =  getAccess(key);
				if (vars[key]['type'] == 'input') {
					$(access).removeClass("input-changed");
					$(access).val(vars[key]['value']);
				} else {
					$(access).html(vars[key]['value']);
				}
			}
			if (isChartShown()) {
				updateChart(vars, key);
			}
		}
	});

	if (isChartShown()) {
		removeOldValuesFromChart();
		chart.update();
	}
}

function updateInputFields(vars, keys) {
	for (i = 0; i < inputFields.length; i++) {
		var key = inputFields[i];
		if (oldVars == null || vars[key]['value'] != oldVars[key]['value']) {
			$(getAccess(key))
				.removeClass("input-changed")
				.val(vars[key]['value']);
		}
	}
}

function getCurrentValues() {
	var values = new Object();
	if (oldVars != null) {
		Object.keys(oldVars).forEach(
			function(key) {
				values[key] = oldVars[key]['value'];
			}
		);
		Object.freeze(values);
	}
	return values;
}

function invokeCallbacks(vars, keys) {
	var changed = false;

	keys.forEach(function(key) {
		if (oldVars == null || vars[key]['value'] != oldVars[key]['value']) {
			changed = true;
			if (callbackMap.has(key)) {
				var callbacks = callbackMap.get(key);
				for (i = 0; i < callbacks.length; i++) {
					callbacks[i](vars[key]['value']);
				}
			}
		}
	});

	if (changed && callbacks.length > 0) {
		var values = new Object()
		keys.forEach(
			function(key) {
				values[key] = vars[key]['value'];
			}
		);
		Object.freeze(values);
		for (i = 0; i < callbacks.length; i++) {
			callbacks[i](values);
		}
	}
}

function updateVariables() {
	var url = showState ? '/api/get-all' : '/api/get-io';
	$.ajax({
		url: url,
		success: function(json) {
			var vars = JSON.parse(json);
			var keys = Object.keys(vars);
			if ($('#variables').length) {
				if (!init) {
					createDivStructure(vars, keys);
					init = true;
				}
				updateDivContent(vars, keys);
			}
			updateInputFields(vars, keys);
			invokeCallbacks(vars, keys);
			oldVars = vars;
		}
	});
}

/** Start */
$(document).ready(function() {
	$(':checkbox').change(function() {
		init = false;
		oldVars = null;
		showInput = $('#show_input').is(':checked');
		showState = $('#show_state').is(':checked');
		showOutput = $('#show_output').is(':checked');
	});

	if (isChartShown()) {
		initChart();
	} else {
		$("#chart").hide();
	}

	$('.b-input').each(function() {
		registerInputField(this.id);
	});
	$('.b-output').each(function() {
		registerOutput(this.id, this.id);
	})

	updateVariables();
	setInterval(updateVariables, 200);
});

/** 
 * Chart functions
 */
var chart;
var chartYAxixNbr = 0;
var lastPeriod = new Object();
resetLastPeriod();

function isChartShown() {
	return chartShowVariables.length > 0;
}

function initChart() {
	var options = {
		scales: {
			yAxes: [{
				display: true,
				ticks: {
					suggestedMin: 0,
					suggestedMax: 120
				}
			}]
		},
		events: [''], // disable mouse events
		animation: {
			duration: 500
		}
	};

	var datasets = []
	chartShowVariables.forEach(function(v) {
		datasets.push({
			data: [],
			label: v,
			borderColor: chartColors[chartShowVariables.indexOf(v)],
			lineTension: 0,
			fill: false
		});
	});

	var ctx = $("#chart");
	chart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: [],
			datasets: datasets
		},
		options: options
	});
}

function resetLastPeriod() {
	chartShowVariables.forEach(function(e) {
		lastPeriod[e] = -1;
	});
	lastPeriod['__label'] = -1;
}

function updateChart(vars, key) {
	if (chartShowVariables.indexOf(key) >= 0) {
		var period = vars['__period'];
		
		// the server has restarted => reset
		if (lastPeriod['__label'] > period) {
			resetLastPeriod();
		}

		if (lastPeriod[key] < period) {
			var index = chartShowVariables.indexOf(key);
			chart.data.datasets[index].data.push(vars[key]['value']);
			lastPeriod[key] = period;
			if (lastPeriod['__label'] < period) {
				chart.data.labels.push(chartYAxixNbr++);
				lastPeriod['__label'] = period;
			}
		}
	}
}

function removeOldValuesFromChart() {
	chart.data.datasets.forEach((dataset) => {
		if (dataset.data.length > 100) {
			dataset.data.shift();
		}
	});
	if (chart.data.labels.length > 100) {
		chart.data.labels.shift();
	}
	chart.update();
}

