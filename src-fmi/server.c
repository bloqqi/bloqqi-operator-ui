#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include "web_server.h"
#include "ControlSystemMain.h"
#define BUF_SIZE 1024

char *fmu_file;
char *temp_dir;

void set_tmp_dir(char *filename) {
	temp_dir = malloc(BUF_SIZE);
	realpath(filename, temp_dir);
	char *p = strrchr(temp_dir, '/');
	*p = '\0';
	sprintf(temp_dir+strlen(temp_dir), "/tmp/");

	struct stat info;
	if(stat(temp_dir, &info) != 0) {
		mkdir(temp_dir, 0777);
	}
}

int main(int argc, char *argv[]) {
	if (argc < 3) {
		printf("Usage: %s <port> <fmu_file>\n", argv[0]);
		exit(0);
	}
	fmu_file = argv[2];
	set_tmp_dir(argv[0]);

	pthread_t thread;
	long t = 0;
	int rc = pthread_create(&thread, NULL, run_fmu, (void *)t);
	if (rc){
		printf("ERROR; return code from pthread_create() is %d\n", rc);
		exit(-1);
	}

	int port = atoi(argv[1]);
	server_callbacks *callbacks = malloc(sizeof(server_callbacks));
	callbacks->get_io_json = get_io_json;
	callbacks->get_all_json = get_io_json;
	callbacks->set_input = set_input_variable;
	callbacks->get_period = get_period;
	return start_webserver(port, callbacks);
}
