#ifndef __ControlSystemMain_h_GUARD
#define __ControlSystemMain_h_GUARD
#include "web_server.h"

void *run_fmu(void *threadid);

json_t* get_io_json();
void set_input_variable(const char *name, const char *value);
long get_period();
#endif