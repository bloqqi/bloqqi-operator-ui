#ifndef FMI2_UTILS_H_GUARD
#define FMI2_UTILS_H_GUARD
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <fmilib.h>
#include <JM/jm_portability.h>

fmi2_value_reference_t get_value_ref(fmi2_import_t *fmu, const char *name);
double get_real_ref(fmi2_import_t *fmu, int ref);
double get_real(fmi2_import_t *fmu, const char *name);
void set_real_ref(fmi2_import_t *fmu, int ref, double value);
void set_real(fmi2_import_t *fmu, const char *name, double value);
fmi2_import_t* load_fmu(const char *fmu_file, const char *temp_dir, fmi2_real_t tstart, fmi2_real_t tend);
void free_fmu(fmi2_import_t *fmu);

#endif
