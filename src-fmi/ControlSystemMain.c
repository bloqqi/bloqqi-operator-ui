#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>
#include <stdbool.h>
#include "fmi2-utils.h"
#include "ControlSystemMain.h"

#define HZ (1)
#define T (1000000/HZ)
#define T_START (0.0)
#define T_STEP (1)
#define T_END (10000.0)

static pthread_mutex_t mutex;

extern char *fmu_file;
extern char *temp_dir;


typedef struct {
	fmi2_value_reference_t vr;
	const char *name;
	double value;
} value_t;

typedef struct {
	value_t *inputs;
	value_t *outputs;
	size_t size_inputs;
	size_t size_outputs;
	long period_index;
} state_t;

static volatile state_t state = {};

long get_period() {
	long retval;
	pthread_mutex_lock(&mutex);
	retval = state.period_index;
	pthread_mutex_unlock(&mutex);
	return retval;
}

json_t* get_io_json() {
	// Copy state before creating string
	state_t state_copy;
	pthread_mutex_lock(&mutex);
	state_copy = state;
	pthread_mutex_unlock(&mutex);

	int n = state_copy.size_inputs + state_copy.size_outputs;
	const int MAX_BUF = 256*(n+1);
	char *c = malloc(MAX_BUF);
	int len = 0;

	len += snprintf(c+len, MAX_BUF-len, "{");
	for (size_t i = 0; i < state_copy.size_inputs; i++) {
		len += snprintf(c+len, MAX_BUF-len, 
			"\"%s\": {\"value\": %f, \"type\": \"input\"}, ",
			state_copy.inputs[i].name, state_copy.inputs[i].value);
	}
	for (size_t i = 0; i < state_copy.size_outputs; i++) {
		len += snprintf(c+len, MAX_BUF-len, 
			"\"%s\": {\"value\": %f, \"type\": \"output\"}, ",
			state_copy.outputs[i].name, state_copy.outputs[i].value);		
	}
	snprintf(c+len, MAX_BUF-len, "\"__period\": %ld}", 
		state_copy.period_index);

	json_t *json = malloc(sizeof(json_t));
	json->s = c;
	json->period = state_copy.period_index;

	return json;
}

void set_input_variable(const char *name, const char *value) {
	pthread_mutex_lock(&mutex);
	for (size_t i = 0; i < state.size_inputs; i++) {
		if (strcmp(name, state.inputs[i].name) == 0) {
			state.inputs[i].value = atof(value);
			break;
		}
	}
	pthread_mutex_unlock(&mutex);
}

static useconds_t useconds() {
	struct timeval v;
	gettimeofday(&v, NULL);
	return v.tv_sec*1000000 + v.tv_usec;
}

void initialize_state(fmi2_import_t *fmu) {
	fmi2_import_variable_list_t *vars;
	vars = fmi2_import_get_variable_list(fmu, 0);
	size_t size = fmi2_import_get_variable_list_size(vars);
	
	// Count number of inputs and outputs
	size_t size_inputs = 0;
	size_t size_outputs = 0;
	for (size_t i = 0; i < size; i++) {
		fmi2_import_variable_t *v = fmi2_import_get_variable(vars, i);
		enum fmi2_causality_enu_t c = fmi2_import_get_causality(v);
		switch (c) {
			case fmi2_causality_enu_input:
				size_inputs++;
				break;
			case fmi2_causality_enu_output:
				size_outputs++;
				break;
			default:
				break;
		}
	}

	// Allocate heap memory
	state.inputs = calloc(sizeof(value_t), size_inputs);
	state.outputs = calloc(sizeof(value_t), size_outputs);
	state.size_inputs = size_inputs;
	state.size_outputs = size_outputs;

	// Read input and output variables
	size_t i_input = 0;
	size_t i_output = 0;
	for (size_t i = 0; i < size; i++) {
		fmi2_import_variable_t *v = fmi2_import_get_variable(vars, i);
		fmi2_value_reference_t vr = fmi2_import_get_variable_vr(v);
		enum fmi2_causality_enu_t c = fmi2_import_get_causality(v);
		switch (c) {
			case fmi2_causality_enu_input:
				state.inputs[i_input].vr = vr;
				state.inputs[i_input].name = fmi2_import_get_variable_name(v);
				state.inputs[i_input].value = 0.0; 
				i_input++;
				break;
			case fmi2_causality_enu_output:
				state.outputs[i_output].vr = vr;
				state.outputs[i_output].name = fmi2_import_get_variable_name(v);
				state.outputs[i_output].value = 0.0; 
				i_output++;
				break;
			default:
				break;
		}
	}
}


void *run_fmu(void *threadid) {
	threadid = NULL;  // Silent warning message...
	
	fmi2_import_t *fmu = load_fmu(fmu_file, temp_dir, T_START, T_END);

	initialize_state(fmu);

	useconds_t extra = 0;
	fmi2_real_t tcur = 0;
	while(true) {
		useconds_t start = useconds();

		// Read input
		pthread_mutex_lock(&mutex);
		for (size_t i = 0; i < state.size_inputs; i++) {
			set_real_ref(fmu, state.inputs[i].vr, state.inputs[i].value);
		}
		pthread_mutex_unlock(&mutex);
			
		// Run FMU one time step
		fmi2_status_t status = fmi2_import_do_step(fmu, tcur, T_STEP, fmi2_true);
		if (status != fmi2_status_ok) {
			printf("Error. FMU could not take one step.\n");
		}
		// Take one time step
		tcur += T_STEP;

		// Store output
		pthread_mutex_lock(&mutex);
		for (size_t i = 0; i < state.size_outputs; i++) {
			state.outputs[i].value = get_real_ref(fmu, state.outputs[i].vr);
		}
		state.period_index++;
		pthread_mutex_unlock(&mutex);

		useconds_t end = useconds();
		useconds_t run_time = end - start;

		// Sleep
		start = end;
		useconds_t sleep_time = T - run_time - extra;
		assert(sleep_time > 0);
		usleep(sleep_time);
		end = useconds();
		useconds_t time_slept = end - start;
		if (run_time + time_slept > T) {
			extra = run_time + time_slept - T;
		} else {
			extra = 0;
		}
	}

	pthread_exit(NULL);
}
