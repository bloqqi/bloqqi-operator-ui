/*
   Copyright (C) 2012 Modelon AB

   This program is free software: you can redistribute it and/or modify
   it under the terms of the BSD style license.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   FMILIB_License.txt file for more details.

   You should have received a copy of the FMILIB_License.txt file
   along with this program. If not, contact Modelon AB <http://www.modelon.com>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <fmilib.h>
#include <JM/jm_portability.h>

#define BUFFER 1000
void importlogger(jm_callbacks * c, jm_string module,
		  jm_log_level_enu_t log_level, jm_string message)
{
	printf("module = %s, log level = %s: %s\n", module,
	       jm_log_level_to_string(log_level), message);
}

/* Logger function used by the FMU internally */
void fmilogger(fmi2_component_t c, fmi2_string_t instanceName,
	       fmi2_status_t status, fmi2_string_t category,
	       fmi2_string_t message, ...)
{

	/* int len;
	   char msg[BUFFER]; */
	va_list argp;
	va_start(argp, message);

	/* len = jm_vsnprintf(msg, BUFFER, message, argp); */
	fmi2_log_forwarding_v(c, instanceName, status, category, message, argp);
	va_end(argp);
}

fmi2_value_reference_t get_value_ref(fmi2_import_t *fmu, const char *name)
{
	fmi2_import_variable_t* v = fmi2_import_get_variable_by_name(fmu, name);
	if (v == NULL) {
		printf("Error! Cannot find variable '%s' in FMU.\n", name);
		exit(1);
	}
	fmi2_value_reference_t t = fmi2_import_get_variable_vr(v);
	return t;
}

double get_real_ref(fmi2_import_t *fmu, int ref)
{
	fmi2_real_t val;
	fmi2_value_reference_t vr = ref;
	fmi2_import_get_real(fmu, &vr, 1, &val);
	return val;
}

double get_real(fmi2_import_t *fmu, const char *name)
{
	return get_real_ref(fmu, get_value_ref(fmu, name));
}

void set_real_ref(fmi2_import_t *fmu, int ref, double value)
{
	fmi2_value_reference_t vr = ref;
	fmi2_real_t v = value;
	fmi2_import_set_real(fmu, &vr, 1, &v);
}

void set_real(fmi2_import_t *fmu, const char *name, double value)
{
	set_real_ref(fmu, get_value_ref(fmu, name), value);
}

void create_instance(fmi2_import_t * fmu, fmi2_real_t tstart, fmi2_real_t tend)
{
	fmi2_string_t instanceName = "Model instance";

	fmi2_string_t fmuLocation = NULL;
	fmi2_boolean_t visible = fmi2_false;
	fmi2_real_t relativeTol = 1e-4;

	fmi2_boolean_t StopTimeDefined = fmi2_false;
	printf("Version returned from FMU:   %s\n", fmi2_import_get_version(fmu));
	printf("Platform type returned:      %s\n", fmi2_import_get_types_platform(fmu));
	printf("GUID:      %s\n", fmi2_import_get_GUID(fmu));

	jm_status_enu_t jmstatus = fmi2_import_instantiate(fmu, instanceName, fmi2_cosimulation, fmuLocation, visible);
	if (jmstatus == jm_status_error) {
		printf("fmi2_import_instantiate failed\n");
		exit(0);
	}
	fmi2_status_t fmistatus = fmi2_import_setup_experiment(fmu, fmi2_true, relativeTol, tstart, StopTimeDefined, tend);
	if (fmistatus != fmi2_status_ok) {
		printf("fmi2_import_setup_experiment failed\n");
		exit(0);
	}
	fmistatus = fmi2_import_enter_initialization_mode(fmu);
	if (fmistatus != fmi2_status_ok) {
		printf("fmi2_import_enter_initialization_mode failed\n");
		exit(0);
	}
	fmistatus = fmi2_import_exit_initialization_mode(fmu);
	if (fmistatus != fmi2_status_ok) {
		printf("fmi2_import_exit_initialization_mode failed\n");
		exit(0);
	}
}

static jm_callbacks callbacks = {
	.malloc = malloc,
	.calloc = calloc,
	.realloc = realloc,
	.free = free,
	.logger = importlogger,
	//.log_level = jm_log_level_debug,
	.log_level = jm_log_level_warning,
	.context = 0
};

fmi2_import_t* load_fmu(const char *fmu_file, const char *temp_dir, fmi2_real_t tstart, fmi2_real_t tend)
{
	fmi_import_context_t *context = fmi_import_allocate_context(&callbacks);
	fmi_version_enu_t version = fmi_import_get_fmi_version(context, fmu_file, temp_dir);
	if (version != fmi_version_2_0_enu) {
		printf("The code only supports version 2.0 (got: %s)\n", fmi_version_to_string(version));
		exit(0);
	}

	fmi2_import_t *fmu = fmi2_import_parse_xml(context, temp_dir, 0);
	if (!fmu) {
		printf("Error parsing XML, exiting\n");
		exit(0);
	}
	if (fmi2_import_get_fmu_kind(fmu) != fmi2_fmu_kind_cs) {
		printf("Only CS 2.0 is supported by this code\n");
		exit(0);
	}
	
	jm_status_enu_t status = fmi2_import_create_dllfmu(fmu, fmi2_fmu_kind_cs, NULL);
	if (status == jm_status_error) {
		printf("Could not create the DLL loading mechanism(C-API) (error: %s).\n",
			fmi2_import_get_last_error(fmu));
		exit(0);
	}

	fmi_import_free_context(context);

	create_instance(fmu, tstart, tend);

	return fmu;
}

void free_fmu(fmi2_import_t *fmu)
{
	fmi2_import_terminate(fmu);
	fmi2_import_free_instance(fmu);

	fmi2_import_destroy_dllfmu(fmu);
	fmi2_import_free(fmu);
}
