Generate a web-based user interface for Bloqqi programs
===============================

Installation
-------------------------------
The web-based user interface uses the HTTP server [Libmicrohttpd](https://www.gnu.org/software/libmicrohttpd/), which can be installed as follows:

    # Install on MacOS
    $ brew install libmicrohttpd

    # Install on Debian
    $ apt-get install libmicrohttpd10 libmicrohttpd-dev 


Usage 
-------------------------------

    # Generate web server given a Bloqqi Program
	$ ./build_webserver examples/TwoCounters.dia 

    # Run generated server on port 8000
	$ ./server 8000
	
Then surf to [http://localhost:8000/](http://localhost:8000/) using a web browser.

FMI support 
-------------------------------
A user interface can also be generated for FMUs as follows.

    # Generate web server
    $ ./build_fmu_webserver

    # Run generated server for given FMU on port 8000
    $ ./fmu_server 8000 file.fmu

Other 
-------------------------------
	# Input values can be set from the command line using curl
    $ curl -d "id=in1&value=2" -X POST http://localhost:8000/api/set-input
